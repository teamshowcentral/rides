package de.kitt3120.rides.utils;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_12_R1.Packet;

public class PacketUtils {
	
	public static void toAll(Packet<?>... packets) {
		for(Packet<?> packet : packets) for(Player player : Bukkit.getOnlinePlayers()) ((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);	
	}
	
	public static void to(List<Player> players, Packet<?>... packets) {
		for(Packet<?> packet : packets) for(Player player : players) ((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);
	}
	
	public static void to(Player[] players, Packet<?>... packets) {
		for(Packet<?> packet : packets) for(Player player : players) ((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);
	}

}

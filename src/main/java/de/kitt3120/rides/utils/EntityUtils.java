package de.kitt3120.rides.utils;

import java.lang.reflect.Method;

import net.minecraft.server.v1_12_R1.EntityLiving;
import net.minecraft.server.v1_12_R1.EntityTypes;

public class EntityUtils {
	
	public static void registerEntity(String name, int id, Class<? extends EntityLiving> customClass, String displayName) {
        try {
            Method method = EntityTypes.class.getDeclaredMethod("a", new Class[]{int.class, String.class, Class.class, String.class});
            method.setAccessible(true);
            method.invoke(EntityTypes.class, id, name, customClass, displayName);
        } catch (Exception ex) {
            //unable to create for some reason, check the stack trace
            ex.printStackTrace();
        }
    }
	
}

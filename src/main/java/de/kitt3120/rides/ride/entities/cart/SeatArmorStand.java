package de.kitt3120.rides.ride.entities.cart;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;

import de.kitt3120.rides.utils.PacketUtils;
import net.minecraft.server.v1_12_R1.EntityArmorStand;
import net.minecraft.server.v1_12_R1.EnumItemSlot;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntity.PacketPlayOutEntityLook;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntity.PacketPlayOutRelEntityMove;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityEquipment;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityMetadata;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityTeleport;
import net.minecraft.server.v1_12_R1.PacketPlayOutSpawnEntityLiving;
import net.minecraft.server.v1_12_R1.World;

public class SeatArmorStand extends EntityArmorStand {
	
	public static ArrayList<SeatArmorStand> allVisibleStands = new ArrayList<>();
	
	private boolean isVisible = false;
	
	public SeatArmorStand(World world) {
		super(world);
		this.setBasePlate(false);
		this.setArms(false);
		this.setInvulnerable(true);
		this.setInvisible(false);
	}

	public SeatArmorStand(Location location) {
		super(((CraftWorld) location.getWorld()).getHandle(), location.getX(), location.getY(), location.getZ());
		this.setBasePlate(false);
		this.setArms(false);
		this.setInvulnerable(true);
		this.setInvisible(false);
	}
	
	public void show() {
		PacketUtils.toAll(new PacketPlayOutSpawnEntityLiving(this));
		PacketUtils.toAll(new PacketPlayOutEntityEquipment(this.getId(), EnumItemSlot.HEAD, this.getEquipment(EnumItemSlot.HEAD)));
		PacketUtils.toAll(new PacketPlayOutEntityMetadata(this.getId(), this.getDataWatcher(), false));
		allVisibleStands.add(this);
		isVisible = true;
	}
	
	public void hide() {
		PacketUtils.toAll(new PacketPlayOutEntityDestroy(this.getId()));
		allVisibleStands.remove(this);
		isVisible =  false;
	}
	
	public void teleport(Location location) {
		boolean useRel = (Math.abs(location.getX() - this.getX()) <= 8 && Math.abs(location.getY() - this.getY()) <= 8 && Math.abs(location.getZ() - this.getZ()) <= 8);
		this.setLocation(location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());
		
		if (useRel) {
            long relX = (long) ((location.getX() * 32 - this.getX() * 32) * 128);
            long relY = (long) ((location.getY() * 32 - this.getY() * 32) * 128);
            long relZ = (long) ((location.getZ() * 32 - this.getZ() * 32) * 128);
            PacketUtils.toAll(new PacketPlayOutRelEntityMove(this.getId(), relX, relY, relZ, false));
        } else PacketUtils.toAll(new PacketPlayOutEntityTeleport(this));
	}
	
	public void teleport(double x, double y, double z, float yaw, float pitch) {
		boolean useRel = (Math.abs(x - this.getX()) <= 8 && Math.abs(y - this.getY()) <= 8 && Math.abs(z - this.getZ()) <= 8);
		this.setLocation(x, y, z, yaw, pitch);
		
		if (useRel) {
            long relX = (long) ((x * 32 - this.getX() * 32) * 128);
            long relY = (long) ((y * 32 - this.getY() * 32) * 128);
            long relZ = (long) ((z * 32 - this.getZ() * 32) * 128);
            PacketUtils.toAll(new PacketPlayOutRelEntityMove(this.getId(), relX, relY, relZ, false));
        } else PacketUtils.toAll(new PacketPlayOutEntityTeleport(this));
	}
	
	public void teleport(double x, double y, double z) {
		boolean useRel = (Math.abs(x - this.getX()) <= 8 && Math.abs(y - this.getY()) <= 8 && Math.abs(z - this.getZ()) <= 8);
		this.setPosition(x, y, z);
		
		if (useRel) {
            long relX = (long) ((x * 32 - this.getX() * 32) * 128);
            long relY = (long) ((y * 32 - this.getY() * 32) * 128);
            long relZ = (long) ((z * 32 - this.getZ() * 32) * 128);
            PacketUtils.toAll(new PacketPlayOutRelEntityMove(this.getId(), relX, relY, relZ, false));
        } else PacketUtils.toAll(new PacketPlayOutEntityTeleport(this));
	}
	
	//https://naira-project.net/naira-project/NaiRa/blob/master/src/main/java/me/Aeolin/naira/CustomMobs/ComplexEntity/ComplexEntityChild.java
	public void rotate(float yaw, float pitch) {
		this.yaw = yaw;
		this.pitch = pitch;
		byte absYaw = (byte) (int) (yaw * 256.0F / 360.0F);
		byte absPitch = (byte) (int) (pitch * 256.0F / 360.0F);
		PacketUtils.toAll(new PacketPlayOutEntityLook(this.getId(), absYaw, absPitch, false));
	}
	
	//https://naira-project.net/naira-project/NaiRa/blob/master/src/main/java/me/Aeolin/naira/CustomMobs/ComplexEntity/ComplexEntityChild.java
	public void update() {
		PacketPlayOutEntityEquipment head = new PacketPlayOutEntityEquipment(this.getId(), EnumItemSlot.HEAD, this.getEquipment(EnumItemSlot.HEAD));
		PacketPlayOutEntityMetadata meta = new PacketPlayOutEntityMetadata(this.getId(), this.getDataWatcher(), false);
		PacketPlayOutEntityTeleport tp = new PacketPlayOutEntityTeleport(this);
		PacketUtils.toAll(head, meta, tp);
	}

	
	public Location getLocation() {
		return new Location(this.getWorld().getWorld(), this.locX, this.locY, this.locZ);
	}
	
	public void setX(float x) {
		this.setLocation(x, this.locY, this.locZ, this.yaw, this.pitch);
		PacketUtils.toAll(new PacketPlayOutEntityTeleport(this));
	}
	
	public void setY(float y) {
		this.setLocation(this.locX, y, this.locZ, this.yaw, this.pitch);
		PacketUtils.toAll(new PacketPlayOutEntityTeleport(this));
	}
	
	public void setZ(float z) {
		this.setLocation(this.locX, this.locY, z, this.yaw, this.pitch);
		PacketUtils.toAll(new PacketPlayOutEntityTeleport(this));
	}
	
	public void setYaw(float yaw) {
		this.yaw = yaw;
		byte absYaw = (byte) (int) (yaw * 256.0F / 360.0F);
		byte absPitch = (byte) (int) (this.pitch * 256.0F / 360.0F);
		PacketUtils.toAll(new PacketPlayOutEntityLook(this.getId(), absYaw, absPitch, false));
	}
	
	public void setPitch(float pitch) {
		this.pitch = pitch;
		byte absYaw = (byte) (int) (this.yaw * 256.0F / 360.0F);
		byte absPitch = (byte) (int) (pitch * 256.0F / 360.0F);
		PacketUtils.toAll(new PacketPlayOutEntityLook(this.getId(), absYaw, absPitch, false));
	}

	public float getYaw() {
		return this.yaw % 360;
	}
	
	public float getPitch() {
		return this.pitch % 360;
	}
	
	public double getX() {
		return this.locX;
	}
	
	public double getY() {
		return this.locY;
	}
	
	public double getZ() {
		return this.locZ;
	}
	
	public boolean isVisible() {
		return isVisible;
	}

}

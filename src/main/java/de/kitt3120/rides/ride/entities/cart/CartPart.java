package de.kitt3120.rides.ride.entities.cart;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import de.kitt3120.rides.Core;
import de.kitt3120.rides.utils.PacketUtils;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityMetadata;
import net.minecraft.server.v1_12_R1.Vector3f;

public class CartPart {
	
	private ArrayList<CartPart> children;
	private SeatArmorStand seatArmorStand;
	private double rad, phi, theta;
	private CartPart parent;
	private BukkitTask positionScheduler;

	public CartPart(Location location) {
		this.parent = null;
		children = new ArrayList<>();
		seatArmorStand = new SeatArmorStand(location);
		this.rad = 0;
		this.phi = 0;
		this.theta = 0;
		
		startPositionScheduler();
	}
	
	public CartPart(CartPart parent, double rad, double phi, double theta) {
		if(parent == null) {
			Core.getInstance().getLogger().severe("Tried to create child CartPart without parent!");
			return;
		}
		this.parent = parent;
		this.parent.addChild(this);
		children = new ArrayList<>();
		seatArmorStand = new SeatArmorStand(parent.getLocation());
		this.rad = rad;
		this.phi = phi;
		this.theta = theta;
		
		startPositionScheduler();
	}
	
	public void startPositionScheduler() {
		if(positionScheduler == null) {
			positionScheduler = new BukkitRunnable() {
				@Override
				public void run() {
					updatePosition();
				}
			}.runTaskTimerAsynchronously(Core.getInstance(), 0, 1);
		}
	}
	
	public void stopPositionScheduler() {
		if(positionScheduler != null) {
			positionScheduler.cancel();
			positionScheduler = null;
		}
	}
	
	public void updatePosition() {
		if(!isParent()) {
			double totalPitch = seatArmorStand.headPose.getX() + seatArmorStand.pitch % 360;
            double totalYaw = seatArmorStand.headPose.getY() + seatArmorStand.yaw % 360;

            double totalTheta = (totalPitch + 90 + theta) % 360;
            double totalPhi = (totalYaw + 180 + phi) % 360;

            double heightInfluenceFactor = Math.cos(Math.toRadians(phi + 90));

            double diffInfluencePercentage = ((Math.cos(Math.toRadians((phi+90)*2)) + 1) / 2)*100;
            //double diffInfluencePercentage = diffInfluenceFactor*100;

            //Switch Y/Z because "Minecraft.getMinecraft().getLogic().equals("kappa"))" returns true
            double offXMax = rad * Math.cos(Math.toRadians(totalPhi)) * Math.sin(Math.toRadians(totalTheta));
            double offXMin = rad * Math.cos(Math.toRadians(totalPhi));
            //Get the offset's value by percentage
            double offX = -(((offXMin-offXMax)*diffInfluencePercentage)-100*offXMin)/100;

            double offZMax = rad * Math.sin(Math.toRadians(totalPhi)) * Math.sin(Math.toRadians(totalTheta));
            double offZMin = rad * Math.sin(Math.toRadians(totalPhi));
            //Get the offset's value by percentage
            double offZ = -(((offZMin-offZMax)*diffInfluencePercentage)-100*offZMin)/100;

            double offY = rad * Math.cos(Math.toRadians(totalTheta)) * heightInfluenceFactor;

            double parentX = (parent == null ? 0 : parent.getSeatArmorStand().getX());
            double parentY = (parent == null ? 0 : parent.getSeatArmorStand().getY());
            double parentZ = (parent == null ? 0 : parent.getSeatArmorStand().getZ());
            float parentYaw = (parent == null ? 0 : parent.getSeatArmorStand().getYaw());
            float parentPitch = (parent == null ? 0 : parent.getSeatArmorStand().getPitch());
            
            double x = parentX + offX;
            double y = parentY + offY;
            double z = parentZ + offZ;
            
            seatArmorStand.teleport(x, y, z, parentYaw, parentPitch);
		}
	}
	
	public void show(boolean deep) {
		seatArmorStand.show();
		if(deep) for(CartPart child : children) child.show(true);
	}
	
	public void hide(boolean deep) {
		seatArmorStand.hide();
		if(deep) for(CartPart child : children) child.hide(true);
	}

	public void teleport(Location location) {
		if(isParent()) {
			seatArmorStand.teleport(location);
		} else {
			Core.getInstance().getLogger().warning("Tried to teleport a child cartpart!");
		}
	}

	public void teleport(double x, double y, double z, float yaw, float pitch) {
		if(isParent()) {
			seatArmorStand.teleport(x, y, z, yaw, pitch);
		} else {
			Core.getInstance().getLogger().warning("Tried to teleport a child cartpart!");
		}
	}
	
	public void setRotation(float yaw, float pitch) {
		seatArmorStand.rotate(yaw, pitch);
	}
	
	public void addRotation(float yaw, float pitch) {
		seatArmorStand.rotate(seatArmorStand.yaw + yaw, seatArmorStand.pitch + pitch);
	}
	
	public float getYaw() {
		return seatArmorStand.getYaw();
	}
	
	public float getPitch() {
		return seatArmorStand.getPitch();
	}
	
	public void setContentRotation(Vector3f rotation) {
		seatArmorStand.setHeadPose(rotation);
		PacketUtils.toAll(new PacketPlayOutEntityMetadata(seatArmorStand.getId(), seatArmorStand.getDataWatcher(), false));
	}
	
	public void setContentRotation(float x, float y, float z) {
		seatArmorStand.setHeadPose(new Vector3f(x, y, z));
		PacketUtils.toAll(new PacketPlayOutEntityMetadata(seatArmorStand.getId(), seatArmorStand.getDataWatcher(), false));
	}
	
	public void addContentRotation(float x, float y, float z) {
		Vector3f newPose = new Vector3f(seatArmorStand.headPose.getX() + x, seatArmorStand.headPose.getY() + y, seatArmorStand.headPose.getZ() + z);
		seatArmorStand.setHeadPose(newPose);
		PacketUtils.toAll(new PacketPlayOutEntityMetadata(seatArmorStand.getId(), seatArmorStand.getDataWatcher(), false));
	}
	
	public void resetContentRotation() {
		seatArmorStand.setHeadPose(new Vector3f(0, 0, 0));
		PacketUtils.toAll(new PacketPlayOutEntityMetadata(seatArmorStand.getId(), seatArmorStand.getDataWatcher(), false));
	}
	
	public Vector3f getContentRotation() {
		return new Vector3f(seatArmorStand.headPose.getX(), seatArmorStand.headPose.getY(), seatArmorStand.headPose.getZ());
	}
	
	public void addChild(CartPart child) {
		if(!children.contains(child)) children.add(child);
	}
	
	public void removeChild(CartPart child) {
		if(children.contains(child)) children.remove(child);
	}
	
	public boolean isParent() {
		return parent == null;
	}
	
	public ArrayList<CartPart> getChildren(boolean deep) {
		if(!deep) return children;
		else {
			ArrayList<CartPart> deepChildren = new ArrayList<>();
			for(CartPart child : children) deepChildren.addAll(child.getChildren(true));
			return deepChildren;
		}
	}
	
	public SeatArmorStand getSeatArmorStand() {
		return seatArmorStand;
	}
	
	public double getPhi() {
		return phi;
	}
	
	public double getRad() {
		return rad;
	}
	
	public double getTheta() {
		return theta;
	}

	public Location getLocation() {
		return seatArmorStand.getLocation();
	}

}

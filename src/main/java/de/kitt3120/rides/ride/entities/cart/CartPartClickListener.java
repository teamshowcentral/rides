package de.kitt3120.rides.ride.entities.cart;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class CartPartClickListener extends Event {
	
	private HandlerList handlerList = new HandlerList();
	
	private CartPart cartPart;
	private Player player;
	
	public CartPartClickListener(CartPart cartPart, Player player) {
		this.cartPart = cartPart;
		this.player = player;
	}
	
	public CartPart getCartPart() {
		return cartPart;
	}
	
	public Player getPlayer() {
		return player;
	}

	@Override
	public HandlerList getHandlers() {
		return handlerList;
	}

}

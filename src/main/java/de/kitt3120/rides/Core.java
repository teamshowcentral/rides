package de.kitt3120.rides;

import org.bukkit.Effect;
import org.bukkit.Particle;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import de.kitt3120.rides.ride.entities.cart.CartPart;
import de.kitt3120.rides.ride.entities.cart.SeatArmorStand;
import de.kitt3120.rides.utils.EntityUtils;

public class Core extends JavaPlugin {
	
	private static Core instance;
	
	@Override
	public void onEnable() {
		instance = this;
		
		EntityUtils.registerEntity("seat_armor_stand", 78, SeatArmorStand.class, "Seat ArmorStand");
	}
	
	public static Core getInstance() {
		return instance;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(command.getName().equalsIgnoreCase("CartTest")) {
			if(sender instanceof Player) {
				Player p = (Player) sender;
				CartPart mainPart = new CartPart(p.getLocation());
				CartPart childPart = new CartPart(mainPart, 1, 0, 0);
				
				mainPart.show(true);
				 
				new BukkitRunnable() {
					
					@Override
					public void run() {
						mainPart.addRotation(10, 0);

						mainPart.getLocation().getWorld().spigot().playEffect(mainPart.getLocation(), Effect.FLAME, 0, 0, 0, 0, 0, 0, 1, 100);
						mainPart.getLocation().getWorld().spigot().playEffect(childPart.getLocation(), Effect.FLAME, 0, 0, 0, 0, 0, 0, 1, 100);
					}
				}.runTaskTimer(this, 0, 1);
			} else {
				sender.sendMessage("Nope");
			}
			
			return true;
		}
		return false;
	}

}
